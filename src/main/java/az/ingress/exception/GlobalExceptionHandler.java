package az.ingress.exception;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.security.SignatureException;
import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorDetails> resourceNotFoundException(ResourceNotFoundException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.NOT_FOUND.value()).
                code(ErrorCodes.RESOURCE_NOT_FOUND.getCode()).
                message(ex.getMessage()).
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorDetails> badRequestException(HttpMessageNotReadableException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.BAD_REQUEST.value()).
                code(ErrorCodes.BAD_REQUEST.getCode()).
                message("Invalid JSON format in the request body").
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceAlreadyExistException.class)
    public ResponseEntity<ErrorDetails> resourceAlreadyExistException(ResourceAlreadyExistException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.BAD_REQUEST.value()).
                code(ErrorCodes.USERNAME_ALREADY_EXIST.getCode()).
                message(ex.getMessage()).
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ErrorDetails> usernameNotFoundException(UsernameNotFoundException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.NOT_FOUND.value()).
                code(ErrorCodes.USERNAME_NOT_FOUND.getCode()).
                message(ex.getMessage()).
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<ErrorDetails> authenticationException(AuthenticationException ex, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder().
                timestamp(new Date()).
                status(HttpStatus.UNAUTHORIZED.value()).
                code(ErrorCodes.NOT_AUTHORIZED.getCode()).
                message(ex.getMessage()).
                description(request.getDescription(false))
                .build();
        return new ResponseEntity<>(errorDetails, HttpStatus.UNAUTHORIZED);
    }

}
