package az.ingress.service;

import az.ingress.dto.UserDto;
import az.ingress.exception.ResourceAlreadyExistException;
import az.ingress.exception.ResourceNotFoundException;
import az.ingress.mapper.UserMapper;
import az.ingress.model.Role;
import az.ingress.model.User;
import az.ingress.enums.UserRole;
import az.ingress.repository.RoleRepository;
import az.ingress.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;


    public UserDto registerUser(UserDto userDto) {
        return save(userDto, UserRole.ROLE_USER);
    }

    public UserDto registerAdmin(UserDto userDto) {
        return save(userDto, UserRole.ROLE_ADMIN);
    }

    @Override
    public UserDto save(UserDto userDto, UserRole roleName) {
        Optional<User> byUsername = userRepository.findByUsername(userDto.getUsername());

        byUsername.ifPresent(user -> {
            throw new ResourceAlreadyExistException("User already exists with username: " + userDto.getUsername());
        });

        Role role = roleRepository.findByName(roleName);

        User user = User.builder().name(userDto.getName()).
                username(userDto.getUsername()).
                email(userDto.getEmail()).
                password(passwordEncoder.encode(userDto.getPassword())).
                roles(List.of(role)).
                isEnabled(true).
                isAccountNonLocked(true).
                isAccountNonExpired(true).
                isCredentialsNonExpired(true)
                .build();
        User savedUser = userRepository.save(user);
        return userMapper.mapToUserDto(savedUser);

    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new ResourceNotFoundException("User not found for username: " + username));
    }
}
