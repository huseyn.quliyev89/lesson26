package az.ingress.service;

import az.ingress.dto.JwtAuthResponse;
import az.ingress.dto.JwtRefreshTokenRequest;
import az.ingress.dto.LoginDto;
import az.ingress.model.User;
import az.ingress.security.JwtTokenProvider;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@AllArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;

    @Override
    public JwtAuthResponse login(LoginDto loginDto) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginDto.getUsername(),
                loginDto.getPassword()
        ));

        User byUsername = userService.findByUsername(loginDto.getUsername());

        return jwtTokenProvider.generateToken(byUsername);
    }

    @Override
    public JwtAuthResponse refreshToken(HttpServletRequest request) {
        String token = getTokenFromRequest(request);
        String tokenType = jwtTokenProvider.getTokenType(token);
        if (tokenType.equals("REFRESH"))
        {
            jwtTokenProvider.validateToken(token);
            String username = jwtTokenProvider.getUsername(token);
            User byUsername = userService.findByUsername(username);
            return jwtTokenProvider.generateToken(byUsername);
        }
        else
        {
            throw new JwtException("");
        }
    }

    private String getTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }

        return null;
    }

}
