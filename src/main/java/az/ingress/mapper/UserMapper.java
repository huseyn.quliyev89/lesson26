package az.ingress.mapper;

import az.ingress.dto.UserDto;
import az.ingress.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto mapToUserDto(User user);
    User mapToUser(UserDto userDto);
    List<UserDto> mapToUserDtoList(List<User> userList);
}
